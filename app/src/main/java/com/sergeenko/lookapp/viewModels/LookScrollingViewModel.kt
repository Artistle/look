package com.sergeenko.lookapp.viewModels

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.sergeenko.lookapp.RepositoryImpl
import com.sergeenko.lookapp.adapters.LookAdapter
import com.sergeenko.lookapp.adapters.MyBaseAdapter
import com.sergeenko.lookapp.dataSourses.MyDataSource
import com.sergeenko.lookapp.interfaces.Repository
import com.sergeenko.lookapp.models.Look
import com.sergeenko.lookapp.models.PostResponse
import com.sergeenko.lookapp.sourseFactories.LookDataSourceFactory
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.coroutineContext

class LookScrollingViewModel @ViewModelInject constructor(
    val repository: Repository
) : PaggingListViewModel<Look>(repository) {

    var lastPostion: Int? = null
    var imagePosition: Int? = null

    lateinit var itemsLiveData: LiveData<PostResponse>

    lateinit var testing: Flow<PostResponse>

    init {
        adapter = createAdapter()
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize * 2)
            .setEnablePlaceholders(true)
            .build()
        lookList = LivePagedListBuilder(newsDataSourceFactory, config).build().asFlow()
    }

    var disableScroll = {  }

    //todo
    override fun createAdapter(): MyBaseAdapter<Look> {
        return LookAdapter(
            viewModelScope = viewModelScope, repository = repository,
        ) {
            retry()
        }
    }

    override fun getDataSourceFactory(): MyDataSource<Look> {
        return LookDataSourceFactory(
            repository,
            viewModelScope,
            errorState
        )
    }

    fun onLoad() {

        //var t = repository.authByPhone("")

        //var t = repositoryImpl.getLooks(1)

        //itemsLiveData = t.asLiveData()
//        val config = PagedList.Config.Builder()
//            .setPageSize(pageSize)
//            .setInitialLoadSizeHint(pageSize * 2)
//            .setEnablePlaceholders(false)
//            .build()

        //itemsLiveData = LivePagedListBuilder(getDataSourceFactory(), config).build()



//        itemsLiveData.asFlow().collect()
    }

    fun setScreenHeight(height: Int) {
        adapter.setHeight(height)
    }

}

