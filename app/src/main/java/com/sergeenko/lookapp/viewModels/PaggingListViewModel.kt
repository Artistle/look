package com.sergeenko.lookapp.viewModels

import androidx.lifecycle.viewModelScope
import androidx.paging.PagedList
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.paged.PagedModelAdapter
import com.sergeenko.lookapp.adapters.MyBaseAdapter
import com.sergeenko.lookapp.dataSourses.MyDataSource
import com.sergeenko.lookapp.fastAdapter.LookImageItem
import com.sergeenko.lookapp.fastAdapter.LookItem
import com.sergeenko.lookapp.interfaces.Repository
import com.sergeenko.lookapp.models.Look
import com.sergeenko.lookapp.models.ModelState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

abstract class PaggingListViewModel<U>(repository: Repository) : BaseViewModel(repository) {

    lateinit var adapter: MyBaseAdapter<U>

    //lateinit var fDadapter: PagedModelAdapter<Look,LookItem>

    var isListInited = false

    private var mainAdapter = ItemAdapter<LookImageItem>()
    private var fastAdapter = FastAdapter.with(mainAdapter)

    abstract fun createAdapter(): MyBaseAdapter<U>

    val errorState = MutableStateFlow<ModelState>(ModelState.Success(null))

    lateinit var lookList: Flow<PagedList<U>>

    lateinit var lookListA: Flow<PagedList<Look>>

    lateinit var fAdaper:FastAdapter<LookItem>

    val pageSize = 10
    val newsDataSourceFactory: MyDataSource<U> by lazy {
        getDataSourceFactory()
    }

    abstract fun getDataSourceFactory(): MyDataSource<U>

    fun retry() {
        newsDataSourceFactory.lookDataSourceFlow.value?.invalidate()
    }

    fun collectData(): MyBaseAdapter<U> {
        viewModelScope.launch {
            if (!isListInited)
                lookList.collect {
                    isListInited = true
                    adapter.submitList(it)
                }
//            lookListA.collect {
//                isListInited = true
//                fDadapter.submitList(it)
//            }
        }
        //var fastAdapter = FastAdapter.with(adapter)

        return adapter
    }


    fun collectState(func: (Boolean) -> Unit = {}) {
        viewModelScope.launch {
            errorState.collect {
                //func(it is ModelState.Loading)
                adapter.setState(it)
            }
        }
    }
}
