package com.sergeenko.lookapp.viewModels

import androidx.hilt.lifecycle.ViewModelInject
import com.sergeenko.lookapp.interfaces.Repository
import com.sergeenko.lookapp.models.PostBodyItem

class NewPostViewModel @ViewModelInject constructor(
    private val repository: Repository,
) : BaseViewModel(repository) {

    val list: MutableList<PostBodyItem> = mutableListOf()

    init {
        list.add(PostBodyItem(id = 0, type = "text", content = ""))
    }
}