package com.sergeenko.lookapp.viewModels

import androidx.hilt.lifecycle.ViewModelInject
import com.sergeenko.lookapp.interfaces.Repository

class PostTapeViewModel @ViewModelInject constructor(
    private val repository: Repository,
) : BaseViewModel(repository) {
}