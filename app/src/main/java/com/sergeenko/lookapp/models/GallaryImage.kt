package com.sergeenko.lookapp.models

import android.graphics.drawable.Drawable
import android.net.Uri

data class GallaryImage(
    val file: Uri,
    var isSelected: Boolean = false,
    var drawable: Drawable? = null
)
