package com.sergeenko.lookapp.fastAdapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.sergeenko.lookapp.R
import com.sergeenko.lookapp.models.Look

class LookItem(lookModel: Look) :
    AbstractItem<LookItem.ViewHolder>() {

    var look = lookModel

    override val layoutRes: Int = R.layout.look_view_test

    override val type: Int = layoutRes

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    class ViewHolder(val view: View) : FastAdapter.ViewHolder<LookItem>(view) {

        private var imagesList: RecyclerView = view.findViewById(R.id.img_view)
        private val adapter = ItemAdapter<LookImageItem>()
        private val fastAdapter = FastAdapter.with(adapter)

        override fun bindView(item: LookItem, payloads: List<Any>) {
            item.look.images.map {
                adapter.add(LookImageItem(it))
            }

            imagesList.adapter = fastAdapter
        }

        override fun unbindView(item: LookItem) {

        }
    }
}