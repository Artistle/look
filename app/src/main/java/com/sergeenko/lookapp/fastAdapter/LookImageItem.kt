package com.sergeenko.lookapp.fastAdapter

import android.view.View
import android.widget.ImageView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.sergeenko.lookapp.R
import com.sergeenko.lookapp.models.Image
import com.squareup.picasso.Picasso

class LookImageItem(list: Image) :
    AbstractItem<LookImageItem.ViewHolder>() {

    val imageUrlList = list

    override val layoutRes: Int = R.layout.look_img

    override val type: Int = layoutRes

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    class ViewHolder(view: View) : FastAdapter.ViewHolder<LookImageItem>(view) {

        val imageView: ImageView = view.findViewById(R.id.look_img)

        override fun bindView(item: LookImageItem, payloads: List<Any>) {
            Picasso
                .get()
                .load(item.imageUrlList.url)
                .into(imageView)
        }

        override fun unbindView(item: LookImageItem) {
            TODO("Not yet implemented")
        }

    }
}


