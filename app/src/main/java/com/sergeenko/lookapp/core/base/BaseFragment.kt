package com.sergeenko.lookapp.core.base

import androidx.annotation.LayoutRes
import androidx.lifecycle.ViewModel
import kotlin.reflect.KClass

open class BaseFragment<VM: BaseViewModel>(viewModelClass: KClass<out ViewModel>, @LayoutRes layoutRes: Int) {



}