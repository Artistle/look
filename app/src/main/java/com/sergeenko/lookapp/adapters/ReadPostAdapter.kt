package com.sergeenko.lookapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.sergeenko.lookapp.R
import com.sergeenko.lookapp.models.PostBodyItem
import com.squareup.picasso.Picasso

class ReadPostAdapter(val list: List<PostBodyItem>) :
    RecyclerView.Adapter<ReadPostAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.read_post_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(list[position])


    override fun getItemCount(): Int = list.size

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        val textViewText = view.findViewById<TextView>(R.id.read_post_text)
        val imageView = view.findViewById<TextView>(R.id.read_post_image)
        val progress = view.findViewById<ProgressBar>(R.id.read_post_progress)
        val imgCard = view.findViewById<CardView>(R.id.read_img_card)
        val imgPost = view.findViewById<ImageView>(R.id.read_post_image)

        fun bind(body: PostBodyItem) {

                when (body.type) {
                    "text" -> {
                        //read_post
                        progress.visibility = View.GONE
                        textViewText.text = body.content.toString()
                    }

                    "image" -> {
                        imgCard.visibility = View.VISIBLE
                        loadImageToPost(body.content.toString()) {
                            progress.visibility = View.GONE
                            imgCard.visibility = View.VISIBLE
                        }
                    }
                    "post" -> {

                    }
                }
        }

        private fun loadImageToPost(url: String, load: () -> Unit) {
            Picasso
                .get()
                .load(url)
                .into(imgPost)
            load()
        }
    }
}
