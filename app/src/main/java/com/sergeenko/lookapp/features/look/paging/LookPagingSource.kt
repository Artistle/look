package com.sergeenko.lookapp.features.look.paging

import com.sergeenko.lookapp.RepositoryImpl
import com.sergeenko.lookapp.models.Look
import javax.inject.Inject

//class LookPagingSource @Inject constructor (val look: RepositoryImpl): PagingSource<Int, Look>() {
//
//    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Look> {
//        return try {
//            val nextPage = params.key ?: 1
//            val response = look.getLooks(nextPage)
//
//
//
//            LoadResult.Page(
//                data = response,
//                prevKey = if (nextPage == 1) null else nextPage - 1,
//                nextKey = 1
//            )
//        } catch (e: Exception) {
//            LoadResult.Error(e)
//        }
//    }
//
//    override fun getRefreshKey(state: PagingState<Int, Look>): Int? {
//        TODO("Not yet implemented")
//    }
//}