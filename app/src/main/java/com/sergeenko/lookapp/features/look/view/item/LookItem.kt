package com.sergeenko.lookapp.features.look.view.item

import android.view.View
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem

open class LookItem: AbstractItem<LookItem.ViewHolder>() {
    override val layoutRes: Int
        get() = TODO("Not yet implemented")
    override val type: Int
        get() = TODO("Not yet implemented")

    override fun getViewHolder(v: View): LookItem.ViewHolder {
        TODO("Not yet implemented")
    }


    class ViewHolder(view: View): FastAdapter.ViewHolder<LookItem>(view) {
        override fun bindView(item: LookItem, payloads: List<Any>) {
            TODO("Not yet implemented")
        }

        override fun unbindView(item: LookItem) {
            TODO("Not yet implemented")
        }

    }
}