package com.sergeenko.lookapp.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Spannable
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import android.util.Log
import android.view.*
import android.view.View.OnTouchListener
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.view.inputmethod.InputConnectionWrapper
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.sergeenko.lookapp.DebugFilter
import com.sergeenko.lookapp.R
import com.sergeenko.lookapp.databinding.AdditionalActionsCommentLayoutBinding
import com.sergeenko.lookapp.databinding.CommentsFragmentBinding
import com.sergeenko.lookapp.models.Comment
import com.sergeenko.lookapp.models.Look
import com.sergeenko.lookapp.viewModels.CommentsViewModel
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


@AndroidEntryPoint
class CommentsFragment : BaseFragment<CommentsFragmentBinding>() {

    override val viewModel: CommentsViewModel by viewModels()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setStatusBar()
    }

    override fun restoreState(savedInstanceState: Bundle?) {
        val v = arguments?.getParcelable<Look>("look")
        viewModel.set(arguments?.getParcelable<Look>("look")!!)
    }

    private fun setStatusBar() {
//        val w: Window = requireActivity().window
//        w.statusBarColor = Color.TRANSPARENT
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            w.decorView.systemUiVisibility =
//                View.SYSTEM_UI_FLAG_VISIBLE or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inc() //set status text  light
//        }

        val w: Window = requireActivity().window
        w.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
        w.statusBarColor = Color.TRANSPARENT
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            w.decorView.systemUiVisibility =
                w.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv() //set status text  light
        }
    }

    override fun bind(inflater: LayoutInflater): CommentsFragmentBinding {
        return CommentsFragmentBinding.inflate(inflater)
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onDestroyView() {
        viewModel.invalid()
        super.onDestroyView()
    }

    @SuppressLint("SetTextI18n")
    override fun <T> manageSuccess(obj: T?) {
        when (obj) {
            is Int -> {
                if (obj > 0) {
                    binding.commentsView.scrollToPosition(obj)
                }
            }
            is Pair<*, *> -> {
                (binding.commentInput.editText as ZanyDoubleText).setPrevView(obj.first.toString())
                setAnswerToSelectedComment(obj.first as String)
                showComments()
                lifecycleScope.launch {
                    delay(750)
                    binding.commentsView.smoothScrollToPosition(obj.second as Int)
                }
            }
            is Comment -> {
                showCommentEdit()
            }
            null -> {
                showComments()
                binding.commentInput.editText?.setText("")
            }
        }
    }

    private fun showCommentEdit() {
        binding.toolbar.visibility = View.GONE
        binding.toolbarEditTitle.visibility = View.VISIBLE
        binding.commentSection.visibility = View.GONE
    }

    private fun showComments() {
        binding.toolbar.visibility = View.VISIBLE
        binding.toolbarEditTitle.visibility = View.GONE
        binding.commentSection.visibility = View.VISIBLE
    }

    @SuppressLint("SetTextI18n")
    private fun setAnswerToSelectedComment(obj: String) {
        binding.commentInput.editText!!.setText("$obj ")
        val boldSpan = ForegroundColorSpan(getColor(requireContext(), R.color.pink))
        val start = 0
        val end: Int = binding.commentInput.editText?.editableText.toString().length
        val flag = Spannable.SPAN_INCLUSIVE_INCLUSIVE
        binding.commentInput.editText?.editableText?.setSpan(boldSpan, start, end, flag)

        val boldSpan2 = ForegroundColorSpan(getColor(requireContext(), R.color.black))
        binding.commentInput.editText!!.editableText.setSpan(boldSpan2, end, end, flag)
        binding.commentInput.editText!!.requestFocusFromTouch()

        showKeyBoard(binding.commentInput.editText!!)
        binding.commentInput.editText!!.setSelection(binding.commentInput.editText?.editableText.toString().length)
    }

    @SuppressLint("ClickableViewAccessibility")
    @ExperimentalCoroutinesApi
    override fun setListeners() {
        withBinding {
            loadUserAvatar()
            toolbarEdit.menu.findItem(R.id.report).setOnMenuItemClickListener {
                lifecycleScope.launch(IO) {
                    val isMyComment = viewModel.isMyPost()
                    val isMyAnswer = viewModel.isMyAnswer()
                    withContext(Main) {
                        showAdditionalActions(
                            toolbar, toolbar.x + toolbar.width / 2,
                            isYourComment = isMyComment,
                            isYoursAnswer = isMyAnswer,
                            onClaim = viewModel::claim,
                            onDelete = viewModel::deleteComment
                        )
                    }
                }
                return@setOnMenuItemClickListener true
            }

            toolbarEdit.setNavigationOnClickListener {
                showComments()
                viewModel.clearSelection()
            }

            toolbar.setNavigationOnClickListener {
                findNavController().popBackStack()
            }

            sendButton.setOnClickListener {
                val text = commentInput.editText?.text.toString()
                if (text.isNotEmpty()) {
                    hideKeyboard()
                    viewModel.addComment(text)
                }
            }
            var lastLength = 0
            var canDoDoubleTap = 0

            commentInput.editText?.setOnClickListener {}

            commentInput.editText?.addTextChangedListener {
                if (lastLength > it!!.length) {
                    if (canDoDoubleTap == 1 && it.toString() <= viewModel.userText) {
                        canDoDoubleTap = 0
                        commentInput.editText?.setText("")
                        viewModel.clearSelection()
                    }
                    canDoDoubleTap++
                    Handler(Looper.myLooper()!!).postDelayed({
                        canDoDoubleTap = 0
                    }, 2000)
                }
                lastLength = it.length

                if (it.toString() != "${viewModel.userText} " && it.toString() != viewModel.userText)
                    sendButton.isEnabled = it.isEmpty() != true
                else {
                    sendButton.isEnabled = false
                }
            }

            sendButton.isEnabled = false
            setRV(commentsView)

            commentsView.setOnTouchListener(OnTouchListener { v, event ->
                hideKeyboard()
                false
            })
        }
    }

    private fun loadUserAvatar() {
        lifecycleScope.launch(IO) {
            //var avatar = viewModel.getUser()?.data?.avatar


            /*
            * todo mock nikitaobryvkov39@gmail.com
            *  пока не решён вопрос с аватаром пользователя
            *
            *
            * */
            var avatar = if(viewModel.getUser()?.data?.avatar.equals("")){
                "https://img.gazeta.ru/files3/401/13283401/upload-010-pic905v-895x505-38844.jpg"
            } else {
                viewModel.getUser()?.data?.avatar
            }

            withContext(Main) {
                Picasso.get()
                    .load(avatar)
                    .noPlaceholder()
                    .into(binding.avatar)

                binding.avatar.clipToOutline = true
            }
        }
    }

    private fun setRV(rv: RecyclerView) {
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = viewModel.collectData()
        viewModel.collectState {
            manageLoading(it)
        }
    }

    override fun manageLoading(b: Boolean) {
        if (b) {
            binding.progressBar.visibility = View.VISIBLE
        } else {
            binding.progressBar.visibility = View.GONE
        }
    }

}

fun showAdditionalActions(
    view: View,
    x: Float,
    isYourComment: Boolean,
    isYoursAnswer: Boolean,
    onClaim: (String) -> Unit,
    onDelete: () -> Unit,
) {
    val customLayout =
        AdditionalActionsCommentLayoutBinding.inflate(LayoutInflater.from(view.context))

    customLayout.deleteConfirm.visibility = View.GONE
    if (isYourComment || isYoursAnswer) {
        customLayout.complainDetailed.visibility = View.GONE
        customLayout.complainMyPostDetailed.visibility = View.VISIBLE
        if (isYourComment) {
            customLayout.claim.visibility = View.GONE
        } else {
            customLayout.claim.visibility = View.VISIBLE
        }
    } else {
        customLayout.complainDetailed.visibility = View.VISIBLE
    }


    // create an alert builder
    val width = LinearLayout.LayoutParams.WRAP_CONTENT
    val height = LinearLayout.LayoutParams.WRAP_CONTENT
    val focusable = true // lets taps outside the popup also dismiss it

    val window = PopupWindow(customLayout.root, width, height, focusable)
    window.isOutsideTouchable = true

    customLayout.claim.setOnClickListener {
        customLayout.complainDetailed.visibility = View.VISIBLE
        customLayout.complainMyPostDetailed.visibility = View.GONE
        customLayout.deleteConfirm.visibility = View.GONE
    }

    customLayout.no.setOnClickListener {
        window.dismiss()
    }

    customLayout.yes.setOnClickListener {
        onDelete()
        window.dismiss()
    }

    customLayout.deleteComment.setOnClickListener {
        customLayout.deleteConfirm.visibility = View.VISIBLE
        customLayout.complainDetailed.visibility = View.GONE
        customLayout.complainMyPostDetailed.visibility = View.GONE
    }

    customLayout.complainSend.setOnClickListener {
        window.dismiss()
    }

    customLayout.spam.setOnClickListener {
        onClaim("Спам")
        customLayout.complainDetailed.visibility = View.GONE
        customLayout.complainSend.visibility = View.VISIBLE
    }

    customLayout.shockContent.setOnClickListener {
        onClaim("Шокирующий контент")
        customLayout.complainDetailed.visibility = View.GONE
        customLayout.complainSend.visibility = View.VISIBLE
        customLayout.deleteConfirm.visibility = View.GONE
    }

    window.contentView = customLayout.root
    window.showAtLocation(view, Gravity.TOP, x.toInt(), 0)
}

class ZanyDoubleText : TextInputEditText {

    var textToDel: String = ""

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context!!,
        attrs,
        defStyle
    ) {
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {}
    constructor(context: Context?) : super(context!!) {}

    fun focusPrev() {
        setText("")
    }

    override fun onCreateInputConnection(outAttrs: EditorInfo): InputConnection {
        return ZanyInputConnection(
            super.onCreateInputConnection(outAttrs),
            true
        )
    }

    fun setPrevView(_textToDel: String) {
        textToDel = _textToDel
    }


    private inner class ZanyInputConnection(target: InputConnection?, mutable: Boolean) :
        InputConnectionWrapper(target, mutable) {

        override fun sendKeyEvent(event: KeyEvent): Boolean {
            if (event.action == KeyEvent.ACTION_DOWN && event.keyCode == KeyEvent.KEYCODE_DEL) {
                Log.d(DebugFilter, "sendKeyEvent event has been called")
            }
            return super.sendKeyEvent(event)
        }
    }
}