package com.sergeenko.lookapp.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.core.os.bundleOf
import androidx.core.view.updateLayoutParams
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sergeenko.lookapp.R
import com.sergeenko.lookapp.databinding.AddLinkPopUpBinding
import com.sergeenko.lookapp.databinding.MarkUserPopUpBinding
import com.sergeenko.lookapp.databinding.NewPostFragmentBinding
import com.sergeenko.lookapp.databinding.PhotoEditViewBinding
import com.sergeenko.lookapp.fragments.NewLookFragment.Companion.BROADCAST_ACTION
import com.sergeenko.lookapp.models.PostBodyItem
import com.sergeenko.lookapp.viewHolders.MarkUserViewHolder
import com.sergeenko.lookapp.viewModels.NewPostViewModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.vanniktech.emoji.EmojiEditText
import com.vanniktech.emoji.EmojiPopup
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class NewPostFragment : WhiteStatusBarFragment<NewPostFragmentBinding>() {

    private var emojiPopup: EmojiPopup? = null

    lateinit var bodyTextInput: EmojiEditText

    override val statusBarColor: Int = Color.WHITE

    override val viewModel: NewPostViewModel by viewModels()

    private val service = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            val uri = Uri.parse(intent?.extras?.getString("extra"))

            addPostItem(
                addImageToPost(context, uri),
                PostBodyItem(type = "image", content = uri, id = viewModel.list.size)
            )
            addPostItem(
                getNewEditText(),
                PostBodyItem(type = "text", content = "", id = viewModel.list.size)
            )
        }
    }

    private fun addImageToPost(context: Context?, uri: Uri?): View {
        val inflater = LayoutInflater.from(context)
        val img = PhotoEditViewBinding.inflate(inflater)

        Picasso.get()
            .load(uri)
            .placeholder(R.drawable.look_img_background)
            .into(img.img, object : Callback {
                override fun onSuccess() {
                    try {
                        val windowHeight = activity?.window?.decorView?.height!! * 0.7
                        if (img.root.height > windowHeight) {
                            img.root.updateLayoutParams {

                                height = windowHeight.toInt()
                            }
                        }
                    } catch (e: Exception) {

                    }
                }

                override fun onError(e: java.lang.Exception?) {

                }

            })
        img.root.post {
            img.root.updateLayoutParams {
                height = WRAP_CONTENT
            }
        }

        img.editView.setOnClickListener {

        }

        img.deleteView.setOnClickListener {
            removePostItem(img.root)
        }

        img.root.setPadding(
            0,
            resources.getDimension(R.dimen._12sdp).toInt(),
            0,
            resources.getDimension(R.dimen._12sdp).toInt()
        )
        return img.root
    }

    private fun removePostItem(root: View) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            viewModel.list.removeIf { it.id == root.id }
        } else {
            viewModel.list.removeAt(root.id)
        }
        // todo binding.pageList.removeView(root)
    }

    private fun addPostItem(view: View, postBodyItem: PostBodyItem) {
        // todo binding.pageList.addView(view)
        view.id = postBodyItem.id
        viewModel.list.add(postBodyItem)
    }

    override fun bind(inflater: LayoutInflater): NewPostFragmentBinding {
        return NewPostFragmentBinding.inflate(inflater)
    }

    override fun setListeners() {
        withBinding {
            val intFilter = IntentFilter(BROADCAST_ACTION)
            activity?.registerReceiver(service, intFilter)

            buildPage()

            toolbar.setNavigationOnClickListener {
                findNavController().popBackStack()
            }

            addLookIcon.setOnClickListener {
                findNavController().navigate(
                    R.id.action_global_newLookFragment,
                    bundleOf(
                        NewLookFragment.ADD_LOOK to true
                    )
                )
            }

            addPictureIcon.setOnClickListener {
                findNavController().navigate(
                    R.id.action_global_newLookFragment,
                    bundleOf(
                        NewLookFragment.MAX_SELECTED to 1,
                        NewLookFragment.JUST_PHOTO to true
                    )
                )
            }

            markUserIcon.setOnClickListener {
                bodyTextInput.editableText.append(" @")
                val boldSpan = ForegroundColorSpan(getColor(requireContext(), R.color.pink))
                val start: Int = bodyTextInput.editableText.toString().length - 1
                val end: Int = bodyTextInput.editableText.toString().length
                val flag = Spannable.SPAN_INCLUSIVE_INCLUSIVE
                bodyTextInput.editableText.setSpan(boldSpan, start, end, flag)
                showMarkUserPopUp(it, List(10) { "asdsd $it" }) { nick ->
                    val str = "$nick "
                    bodyTextInput.editableText.append(str)
                    val boldSpan = ForegroundColorSpan(getColor(requireContext(), R.color.pink))
                    val start: Int = bodyTextInput.editableText.toString().length - str.length
                    val end: Int = bodyTextInput.editableText.toString().length
                    val flag = Spannable.SPAN_INCLUSIVE_INCLUSIVE
                    bodyTextInput.editableText.setSpan(boldSpan, start, end, flag)
                    val boldSpan2 = ForegroundColorSpan(getColor(requireContext(), R.color.black))
                    bodyTextInput.editableText.setSpan(boldSpan2, end, end, flag)
                }
            }

            addLinkIcon.setOnClickListener { view ->
                showAddLinkPopUp(view) {
                    bodyTextInput.editableText.append(it)
                    val boldSpan = ForegroundColorSpan(getColor(requireContext(), R.color.pink))
                    val start: Int = bodyTextInput.editableText.toString().length - it.length
                    val end: Int = bodyTextInput.editableText.toString().length
                    val flag = Spannable.SPAN_INCLUSIVE_INCLUSIVE
                    bodyTextInput.editableText.setSpan(boldSpan, start, end, flag)
                    val boldSpan2 = ForegroundColorSpan(getColor(requireContext(), R.color.black))
                    bodyTextInput.editableText.setSpan(boldSpan2, end, end, flag)
                }
            }

            headerHint.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    focusHeader()
            }

            headerTextInput.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    focusHeader()
            }

            setHeaderHintSpannableText()
            setPhotoHintSpannableText()

            binding.headerTextInput.editText?.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    binding.headerHint.visibility = View.GONE
                } else {
                    if (binding.headerTextInput.editText?.text?.isEmpty() == true)
                        binding.headerHint.visibility = View.VISIBLE
                    else {
                        binding.headerHint.visibility = View.GONE
                    }
                }

            }
        }
    }

    private fun buildPage() {
        withBinding {
            viewModel.list.forEachIndexed { index, postBodyItem ->
                when (postBodyItem.type) {
                    "text" -> {
                        val textView = getNewEditText(postBodyItem.content as String)
                        if (index == 0) {
                            textView.setPadding(
                                textView.paddingLeft,
                                resources.getDimension(R.dimen._12sdp).toInt(),
                                textView.paddingRight,
                                textView.paddingBottom
                            )
                            setNewBodyText(textView)
                        }
                        textView.id = index
                        // todo pageList.addView(textView)
                    }
                    "image" -> {
                        val view = addImageToPost(context, postBodyItem.content as Uri)
                        view.id = index
                        // todo pageList.addView(view)
                    }
                    "post" -> {
                        //TODO
                    }
                }
            }
        }
    }

    private fun setNewBodyText(newEditText: EmojiEditText) {
        withBinding {
            bodyTextInput = newEditText

            emojiPopup = EmojiPopup.Builder.fromRootView(root).build(bodyTextInput)
            emojiPopup?.dismiss()

            emojyIcon.setOnClickListener {
                emojiPopup?.toggle()
                emojyIcon.isActivated = !emojyIcon.isActivated
            }

            bodyTextInput.addTextChangedListener {
                try {
                    if (it.toString().last() == ' ') {
                        val end: Int = bodyTextInput.editableText.toString().length
                        val flag = Spannable.SPAN_INCLUSIVE_INCLUSIVE
                        val boldSpan2 =
                            ForegroundColorSpan(getColor(requireContext(), R.color.black))
                        bodyTextInput.editableText.setSpan(boldSpan2, end, end, flag)
                    }
                    viewModel.list.find { element -> element.id == bodyTextInput.id }?.content =
                        it.toString()
                } catch (e: Exception) {

                }
            }
        }
    }

    private fun getNewEditText(string: String = ""): EmojiEditText {
        val text = EmojiEditText(context)
        text.setText(string)
        text.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setNewBodyText(text)
            }
        }
        text.post {
            text.setPadding(
                resources.getDimension(R.dimen._16sdp).toInt(),
                0,//resources.getDimension(R.dimen._12sdp).toInt(),
                resources.getDimension(R.dimen._16sdp).toInt(),
                0//resources.getDimension(R.dimen._12sdp).toInt()
            )
            bodyTextInput.background.setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.SRC_IN);
            text.updateLayoutParams {
                width = MATCH_PARENT
                height = WRAP_CONTENT
            }
        }
        return text
    }

    private fun setPhotoHintSpannableText() {
        val text = getString(R.string.new_post_photo_hint)
        val spannable = SpannableString(text)

        val indexStarStart = text.indexOf("*")
        val indexStarEnd = text.indexOf("*") + "*".length

        val fc = ForegroundColorSpan(getColor(requireContext(), R.color.pink))
        spannable.setSpan(fc, indexStarStart, indexStarEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        binding.photoHint.text = spannable
    }

    private fun setHeaderHintSpannableText() {
        val text = getString(R.string.new_post_header_hint)
        val spannable = SpannableString(text)

        val indexStarStart = text.indexOf("*")
        val indexStarEnd = text.indexOf("*") + "*".length

        val indexSecondStart = text.indexOf("\n")
        val indexSecondEnd = text.length

        val fc = ForegroundColorSpan(getColor(requireContext(), R.color.pink))
        val fc2 = ForegroundColorSpan(getColor(requireContext(), R.color.black_40))
        val rss = RelativeSizeSpan(0.7f)

        spannable.setSpan(fc, indexStarStart, indexStarEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannable.setSpan(fc2, indexSecondStart, indexSecondEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannable.setSpan(rss, indexSecondStart, indexSecondEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        binding.headerHint.text = spannable
    }

    private fun focusHeader() {
        binding.headerTextInput.editText?.requestFocus()
    }

}


inline fun showAddLinkPopUp(view: View, crossinline onSave: (String) -> Unit) {
    val customLayout = AddLinkPopUpBinding.inflate(LayoutInflater.from(view.context))

    // create an alert builder
    val width = LinearLayout.LayoutParams.WRAP_CONTENT
    val height = LinearLayout.LayoutParams.WRAP_CONTENT
    val focusable = true // lets taps outside the popup also dismiss it

    val window = PopupWindow(customLayout.root, width, height, focusable)
    window.isOutsideTouchable = true

    customLayout.cancel.setOnClickListener {
        window.dismiss()
    }

    customLayout.save.setOnClickListener {
        onSave(" ${customLayout.linkInputLayout.editText?.text.toString()} ")
        window.dismiss()
    }

    window.contentView = customLayout.root
    window.showAtLocation(view, Gravity.CENTER, 0, 0)
}

fun showMarkUserPopUp(view: View, userList: List<String>, onSave: (String) -> Unit) {
    val customLayout = MarkUserPopUpBinding.inflate(LayoutInflater.from(view.context))

    // create an alert builder
    val width = LinearLayout.LayoutParams.WRAP_CONTENT
    val height = LinearLayout.LayoutParams.WRAP_CONTENT
    val focusable = true // lets taps outside the popup also dismiss it

    val window = PopupWindow(customLayout.root, width, height, focusable)
    window.isOutsideTouchable = true

    val adapter = MarkUserAdapter {
        onSave(it)
        window.dismiss()
    }

    adapter.setList(userList)
    customLayout.userList.layoutManager = LinearLayoutManager(view.context)
    customLayout.userList.adapter = adapter

    window.contentView = customLayout.root
    window.showAtLocation(view, Gravity.CENTER, 0, 0)
}

class MarkUserAdapter(private val onSave: (String) -> Unit) :
    RecyclerView.Adapter<MarkUserViewHolder>() {

    var userList: List<String> = listOf()

    fun setList(newUserList: List<String>) {
        userList = newUserList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarkUserViewHolder {
        return MarkUserViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.mark_user_element, null, false)
        )
    }

    override fun onBindViewHolder(holder: MarkUserViewHolder, position: Int) {
        holder.bind(userList[position]) {
            onSave(it)
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }

}
