
package com.sergeenko.lookapp.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.*
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.paged.PagedModelAdapter
import com.sergeenko.lookapp.R
import com.sergeenko.lookapp.adapters.LookAdapter
import com.sergeenko.lookapp.databinding.LookScrollingFragmentBinding
import com.sergeenko.lookapp.fastAdapter.LookItem
import com.sergeenko.lookapp.models.Look
import com.sergeenko.lookapp.viewModels.LookScrollingViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class LookScrollingFragment :
    BaseFragment<LookScrollingFragmentBinding>() { // Инициализация класса фрагмента типа BaseFragment с параметром привязанки данных этого фрагмента

    override val viewModel: LookScrollingViewModel by navGraphViewModels(R.id.menu_navigation) { // Инициализация ViewModel для временного хранения данных(?)
        defaultViewModelProviderFactory
    }

    private lateinit var pagedLookList: PagedModelAdapter<Look,LookItem>

    private lateinit var pagedLookAdapter: FastAdapter<LookItem>

    override fun onActivityCreated(savedInstanceState: Bundle?) { // Когда активность создана
        super.onActivityCreated(savedInstanceState)
        setStatusBar() // Функция, которая делает статус бар прозрачным
    }

    private fun setStatusBar() {
        val w: Window = requireActivity().window
        w.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
        w.statusBarColor = Color.TRANSPARENT
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            w.decorView.systemUiVisibility =
                w.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv() //set status text  light
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.lastPostion?.let { binding.rv.scrollToPosition(it) } // Устанавливает значение последнего просмотренного поста при возобновлении фрагмента
        //TODO: viewModel.imagePosition?.let {binding.rv. - - - - - - }
    }

    override fun onStart() {
        super.onStart()
        viewModel.apply {
//            onLoad()
//            itemsLiveData.observe(viewLifecycleOwner) {
//                var t = it
//                var e = ""
//                pagedLookList.submitList(it)
//            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //setUpRecyclerView()
    }

    override fun bind(inflater: LayoutInflater): LookScrollingFragmentBinding {
        return LookScrollingFragmentBinding.inflate(inflater) // Устанавливается привязка данных (?)
    }

    override fun setListeners() {
        withBinding {
            rv.apply {
                val snapHelper = PagerSnapHelper() // Or PagerSnapHelper
                snapHelper.attachToRecyclerView(this)
                //addIndicator()
                setRV(this)
            }
        // С помощью привязки данных\
        // Инициализируется Список RecycleView
        }
    }

    private fun setUpRecyclerView() {
        val asyncDifferConfig = AsyncDifferConfig.Builder(object : DiffUtil.ItemCallback<Look>() {
            override fun areItemsTheSame(oldItem: Look, newItem: Look): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Look, newItem: Look): Boolean {
                return oldItem == newItem
            }
        }).build()

        pagedLookList = PagedModelAdapter<Look, LookItem>(asyncDifferConfig) {

            LookItem(it).apply {  }

        }

        pagedLookAdapter = FastAdapter.with(pagedLookList)

//        withBinding {
//            rv.adapter = pagedLookAdapter
//        }
    }

    private fun setRV(rv: RecyclerView) {
//        val llm = CustomGridLayoutManager(
//            context,
//            RecyclerView.VERTICAL,
//            false
//        )
        // Переменная типа LayoutManager (?)
        //TODO: На некоторых cardView отсуствует радиус, неизвестно почему

        rv.clipToOutline = true

//        (viewModel.adapter as LookAdapter).disableScroll = {
//            binding.cardView.radius = if (it) { // Если disableScroll == true
//                //TODO: Выяснить, от чего зависит disableScroll
//                resources.getDimension(R.dimen._15sdp) // Устанавливает радиус cardView 15dp
//            } else { // Иначе
//                0f // cardView не закругляет углы
//            }
            //llm.setScrollEnabled(it) // Устанавливает текущее значение disableScroll в наш LayoutManager
//        }
        //rv.layoutManager = llm // LayoutManager нашего списка перенимает менеджера с переменной выше

        //val adapter = (viewModel.collectData()) as ItemAdapter<LookItem>
        //val adapter = viewModel.testCollect()
        //rv.adapter = FastAdapter.with(adapter)

        rv.adapter = viewModel.collectData() // Собирает данные (какие?)
        binding.rv.post { // Что это?
            viewModel.setScreenHeight(binding.rv.height) // Сохраняет высоту списка в viewModel
        }
//        if (viewModel.lastPostion == null) // Если нет последнего состояния списка
//            viewModel.lastPostion =
//                (llm.findFirstVisibleItemPosition() + llm.findLastVisibleItemPosition()) / 2
        // Оно устанавливается путём суммы первого и последнего видимого элемента
        // поделённого на 2

        val snapHelper = PagerSnapHelper()

//        binding.rv.addOnScrollListener(object :
//            RecyclerView.OnScrollListener() { // Исполняется, когда пользователь листает список
//
//            var allowToScroll = true // Переменная, отвечающая за возможность скроллинга
//
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                val first: Int = llm.findFirstVisibleItemPosition()
//                val last: Int = llm.findLastVisibleItemPosition()
//                val newPosition =
//                    when {
//                        dy > 0 -> (first + last) / 2
//                        dy < 0 -> (first + last + 1) / 2
//                        else -> viewModel.lastPostion
//                    }
//
//                if (viewModel.adapter.isPostOpen(viewModel.lastPostion!!)) {
//                    //llm.setScrollEnabled(false)
//                    snapHelper.attachToRecyclerView(null)
//                } else {
//                    //llm.setScrollEnabled(true)
//                    snapHelper.attachToRecyclerView(binding.rv)
//                }
//
//                if (newPosition != viewModel.lastPostion && !isCurrentListOpen(dy)) {
//                    if (allowToScroll && recyclerView.isInTouchMode) {
//                        //allowToScroll = false
//
//                        viewModel.adapter.closeLastView(viewModel.lastPostion!!)
//
//                        viewModel.lastPostion = newPosition
//                        //binding.rv.scrollToPosition(newPosition!!)
//                        lifecycleScope.launch {
//                            delay(100)
//                            allowToScroll = true
//                        }
//                    }
//                }
//                super.onScrolled(recyclerView, dx, dy)
//            }
//        })
        viewModel.collectState()
    }

    private fun isCurrentListOpen(dy: Int): Boolean {
        return try {
            viewModel.adapter.currentList?.get(viewModel.lastPostion!!)?.isPostOpen == true && dy < 0
        } catch (e: Exception) {
            false
        }
    }

}

