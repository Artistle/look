data class FavAnswer(
    var `data`: Data = Data(),
    var message: String = "",
    var ok: Boolean = false,
    var status: Int = 0
)